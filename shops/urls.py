from django.conf.urls import url
from shops import views

urlpatterns = [
    url(r'^api/shops$', views.shop_list),
    url(r'^api/shops/(?P<pk>[0-9]+)$', views.shop_detail),
    url(r'^api/shops/opened$', views.shop_list_opened)
]