from rest_framework import serializers
from shops.models import Shop

class ShopSerializer(serializers.ModelSerializer):
    class Meta:
        model = Shop
        fields = (
            'id',
            'network',
            'title',
            'address',
            'open_at',
            'close_at'
        )

