from django.shortcuts import render

from django.http.response import JsonResponse, HttpResponse
from django.utils import timezone

from rest_framework.parsers import JSONParser
from rest_framework import status


from shops.models import Shop
from shops.serializers import ShopSerializer
from rest_framework.decorators import api_view


@api_view(['GET', 'POST', 'DELETE'])
def shop_list(request):
    if request.method == 'GET':
        shops = Shop.objects.all()

        title = request.GET.get('title', None)
        if title is not None:
            shops = shops.filter(title__icontains=title)

        address = request.GET.get('address', None)
        if address is not None:
            shops = shops.filter(address__icontains=address)

        shops_serializer = ShopSerializer(shops, many=True)
        return JsonResponse(shops_serializer.data, safe=False)

    elif request.method == 'POST':
        shop_data = JSONParser().parse(request)
        shop_serializer = ShopSerializer(data=shop_data)
        if shop_serializer.is_valid():
            shop_serializer.save()
            return JsonResponse(shop_serializer.data, status=status.HTTP_201_CREATED)
        return JsonResponse(shop_serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    elif request.method == 'DELETE':
        count = Shop.objects.all().delete()
        return JsonResponse({'message': '{} All shops were deleted successfully!'.format(count[0])}, status=status.HTTP_204_NO_CONTENT)

@api_view(['GET', 'PUT', 'DELETE'])
def shop_detail(request, pk):
    try:
        shop = Shop.objects.get(pk=pk)
    except Shop.DoesNotExist:
        return JsonResponse({'message': 'Shop does not exist'}, status=status.HTTP_404_NOT_FOUND)

    if request.method == 'GET':
        shop_serializer = ShopSerializer(shop)
        return JsonResponse(shop_serializer.data)

    elif request.method == 'PUT':
        shop_data = JSONParser().parse(request)
        shop_serializer = ShopSerializer(shop, data=shop_data)
        if shop_serializer.is_valid():
            shop_serializer.save()
            return JsonResponse(shop_serializer.data)
        return JsonResponse(shop_serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    elif request.method == 'DELETE':
        shop.delete()
        return JsonResponse({'message': 'Shop was deleted successfully!'}, status=status.HTTP_204_NO_CONTENT)

@api_view(['GET'])
def shop_list_opened(request):
    now = timezone.now()
    shops = Shop.objects.filter(open_at__lte=now, close_at__gt=now)

    if request.method == 'GET':
        shop_serializer = ShopSerializer(shops, many=True)
        return JsonResponse(shop_serializer.data, safe=False)
