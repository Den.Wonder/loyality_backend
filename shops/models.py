from django.db import models

# Create your models here.

class Shop(models.Model):
    title = models.CharField(max_length=128, blank=False, default='')
    address = models.CharField(max_length=256, blank=False, default='')
    network = models.CharField(max_length=128, blank=True, default='')
    open_at = models.TimeField(auto_now=False, auto_now_add=False, default='00:00:00')
    close_at = models.TimeField(auto_now=False, auto_now_add=False, default='00:00:00')
