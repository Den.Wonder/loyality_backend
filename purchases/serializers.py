from rest_framework import serializers
from purchases.models import Purchase, Transaction

class TransactionSerializer(serializers.ModelSerializer):

    class Meta:
        model = Transaction
        fields = (
            'id',
            'user_id',
            'shop_id',
            'transaction_datetime',
            'check_amount',
            'check_discount',
            'purchases'
        )




class PurchaseSerializer(serializers.ModelSerializer):

    class Meta:
        model = Purchase
        fields = (
            'id',
            'transaction',
            'product_id',
            'product_count',
            'amount',
            'is_promotion',
            'discount'
        )