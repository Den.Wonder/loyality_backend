import json

from django.shortcuts import render

from django.http.response import JsonResponse
from rest_framework.parsers import JSONParser
from rest_framework import status

from purchases.models import Purchase
from purchases.models import Transaction

from purchases.serializers import PurchaseSerializer
from purchases.serializers import TransactionSerializer

from rest_framework.decorators import api_view

@api_view(['GET', 'POST', 'DELETE'])
def transaction_list(request):
    if request.method == 'GET':
        print('get')
        dates_after = request.GET.get('dates_after', None)
        dates_before = request.GET.get('dates_before', None)
        user_id = request.GET.get('user_id', None)
        shop_id = request.GET.get('shop_id', None)

        transactions = Transaction.objects

        if dates_after is not None:
            transactions = transactions.filter(transaction_datetime__gte=dates_after)

        if dates_before is not None:
            transactions = transactions.filter(transaction_datetime__lte=dates_before)

        if user_id is not None:
            transactions = transactions.filter(user_id=user_id)

        if shop_id is not None:
            transactions = transactions.filter(shop_id=shop_id)

        transaction_serializer = TransactionSerializer(transactions, many=True)
        # return JsonResponse(transaction_serializer.data, safe=False)
        return JsonResponse({'message': 'get'}, status=status.HTTP_200_OK)


    elif request.method == 'POST':
        request_data = JSONParser().parse(request)
        transaction_data = request_data['transaction']
        transaction_serializer = TransactionSerializer(data=transaction_data)
        if transaction_serializer.is_valid():
            transaction_serializer.save()

        purchases_data = request_data['purchases']
        purchases = []
        for purchase in purchases_data:
            purchase_obj = purchase
            purchase_obj.transaction = transaction_serializer.id
            purchases_serializer = PurchaseSerializer(data=purchase_obj)
            if purchases_serializer.is_valid():
                purchases_serializer.save()

        return JsonResponse({'message': 'transaction was saved'}, status=status.HTTP_200_OK)


@api_view(['GET', 'PUT', 'DELETE'])
def transaction_detail(request, pk):
    try:
        transaction = Transaction.objects.get(pk=pk)
    except Transaction.DoesNotExist:
        return JsonResponse({'message':"this transaction does not exist"}, status=status.HTTP_404_NOT_FOUND)

    if request.method == 'GET':
        print('get')

    elif request.method == 'PUT':
        print('put')

    elif request.method == 'DELETE':
        print('delete')