from django.conf.urls import url
from purchases import views

urlpatterns = [
    url(r'^api/transactions$', views.transaction_list),
    url(r'^api/transactions/<uuid:transaction_id>/', views.transaction_detail)
]