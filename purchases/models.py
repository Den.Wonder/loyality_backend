import uuid

from django.db import models

# Create your models here.
import products.models
import shops.models
import users.models


def default_check_purchases_list():
    return []

class Transaction(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    user_id = models.ForeignKey(users.models.User, on_delete=models.SET_DEFAULT, default='unidentifed', blank=False)
    shop_id = models.ForeignKey(shops.models.Shop, on_delete=models.SET_DEFAULT, default=None, blank=False, null=True)
    transaction_datetime = models.DateTimeField(auto_now=False, auto_now_add=True)
    check_amount = models.FloatField(blank=True, default=0.00)
    check_discount = models.FloatField(blank=True, default=0.00)
    purchases = models.JSONField(blank=True, default=default_check_purchases_list)


class Purchase(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    transaction = models.ForeignKey(Transaction, on_delete=models.SET_NULL, default='', blank=False, null=True)
    product_id = models.ForeignKey(products.models.ProductModel, on_delete=models.SET_DEFAULT, default='undefined', blank=False)
    product_count = models.FloatField(blank=False, default=0.00)
    amount = models.FloatField(blank=False, default=0.00)
    is_promotion = models.BooleanField(blank=True, default=False)
    discount = models.FloatField(default=0.00, blank=True)


