from django.contrib import admin
from django.urls import path
from django.conf.urls import url, include
from users import views
from shops import views
from products import views
from purchases import views


urlpatterns = [
    path('admin/', admin.site.urls),
    url(r'^', include('users.urls')),
    url(r'^', include('shops.urls')),
    url(r'^', include('products.urls')),
    url(r'^', include('purchases.urls')),

]
