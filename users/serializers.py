from rest_framework import serializers
from users.models import User

class UserSerializer(serializers.ModelSerializer):

    class Meta:
        model = User
        fields = (
            'id',
            'firstname',
            'secondname',
            'lastname',
            'DoB',
            'gender',
            'married',
            'spouse',
            'parent',
            'childrens',
            'have_animals',
            'animals',
            'work',
            'allergies',
            'preferences',
            'preferred_communication_method',
            'email',
            'phone',
            'viber',
            'whatsapp',
            'telegram',
            'date_of_connect_to_PL',
            'date_of_last_transaction'
        )
