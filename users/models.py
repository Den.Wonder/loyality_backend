from django.db import models

def default_spouse():
    #name of spouse, date of marrige, date of birth
    # 'name':'',
    # 'DoM':'',
    # 'DoB':''
    return {
    }

def default_childrens():
    # {
    #     'name':'',
    #     'gender':'',
    #     'DoB':''
    # },
    return [
    ]

def default_animals():
    #name, DoB, biological species
    # {
    #     'name':'',
    #     'DoB':'',
    #     'species':''
    # }
    return []

def default_allergies():
    #features: do not include this products to preferences list
    return []

def default_preferences():
    return []


class User(models.Model):
    #Name
    firstname = models.CharField(max_length=128, blank=True, default='')
    secondname = models.CharField(max_length=128, blank=True, default='')
    lastname = models.CharField(max_length=128, blank=True, default='')
    #date of birth
    DoB = models.DateField(blank=True, auto_now=False, auto_now_add=False, default='1900-01-01')
    #gender. can be: male, female, none
    gender = models.CharField(max_length=6, blank=True, default='none')

    #marital status ()
    married = models.BooleanField(default=False)
    #married
    spouse = models.JSONField(default=default_spouse)

    #have a childrens
    parent = models.BooleanField(default=False)
    #childrens
    childrens = models.JSONField(default=default_childrens)

    #have an animals
    have_animals = models.BooleanField(default=False)
    animals = models.JSONField(default=default_animals)

    #work status
    # student, unemployed, employer, businessman, state, goverment, retired
    work = models.CharField(max_length=64, blank=True, default='')

    #some individual specials (list of products categories, with DO NOT INCLUDE, and list of preferences products categories)
    allergies = models.JSONField(default=default_allergies)
    preferences = models.JSONField(default=default_preferences)

    #preferred_communication_method - choosen method of inform
    #phone, email, viber, whatsapp, telegram
    preferred_communication_method = models.CharField(max_length=64, blank=True, default='')
    #contacts
    email = models.EmailField(max_length=255, blank=True, default='')
    phone = models.CharField(max_length=11, blank=True, default='')
    viber = models.CharField(max_length=128, blank=True, default='')
    whatsapp = models.CharField(max_length=128, blank=True, default='')
    telegram = models.CharField(max_length=128, blank=True, default='')

    #User category


    #activity information
    date_of_connect_to_PL = models.DateField(auto_now_add=True)
    date_of_last_transaction = models.DateField(auto_now=True)

