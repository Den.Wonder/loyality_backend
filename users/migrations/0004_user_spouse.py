# Generated by Django 3.2 on 2021-05-09 18:13

from django.db import migrations, models
import users.models


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0003_auto_20210509_1807'),
    ]

    operations = [
        migrations.AddField(
            model_name='user',
            name='spouse',
            field=models.JSONField(default=users.models.default_spouse),
        ),
    ]
