from django.conf.urls import url
from products import views

urlpatterns = [
    url(r'^api/products$', views.product_list),
    url(r'^api/products/(?P<pk>[0-9]+)$', views.product_detail),
    url(r'^api/products/by-category/(?P<categoty_id>[0-9]+$)', views.product_list_by_category),
    url(r'^api/products/by-feature/(?P<feature_id>[0-9]+$)', views.product_list_by_feature),
    url(r'^api/categories$', views.category_list),
    url(r'^api/categories/(?P<pk>[0-9]+)$', views.category_detail),
    url(r'^api/features$', views.feature_list),
    url(r'^api/features/(?P<pk>[0-9]+)$', views.feature_detail),


]