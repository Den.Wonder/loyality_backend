from django.db import models


class ProductCategoryModel(models.Model):
    title = models.CharField(max_length=128, blank=False, default='')
    description = models.CharField(max_length=256, blank=False, default='')


class ProductFeatureModel(models.Model):
    title = models.CharField(max_length=128, blank=True, default='')
    description = models.CharField(max_length=256, blank=True, default='')


class ProductModel(models.Model):
    title = models.CharField(max_length=128, blank=False, default='')
    value = models.DecimalField(max_digits=7, decimal_places=3, blank=True, default=1.000)
    measuring = models.CharField(max_length=32, blank=True, default='piece')
    trade_price = models.FloatField(blank=False, default=0.00)
    retail_price = models.FloatField(blank=False, default=0.00)
    max_discount_percent = models.FloatField(blank=True, default=0.00)
    category = models.ForeignKey(to=ProductCategoryModel, on_delete=models.SET_DEFAULT, default='', blank=True)
    #может, это все-таки должен быть jsonfield?
    feature = models.ForeignKey(to=ProductFeatureModel, on_delete=models.SET_DEFAULT, blank=True, default='')

