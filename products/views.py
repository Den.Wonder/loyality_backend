from django.shortcuts import render

from django.http.response import JsonResponse
from rest_framework.parsers import JSONParser
from rest_framework import status

from products.models import ProductModel, ProductFeatureModel, ProductCategoryModel
from products.serializers import ProducModelSerializer, ProductCategoryModelSerializer, ProductFeatureModelSerializer
from rest_framework.decorators import api_view

@api_view(['GET', 'POST', 'DELETE'])
def product_list(request):
    if request.method == 'GET':
        products = ProductModel.objects.all()

        title = request.GET.get('title', None)
        if title is not None:
            products = products.filter(title__icontains=title)

        product_serializer = ProducModelSerializer(products, many=True)
        return JsonResponse(product_serializer.data, safe=False)

    elif request.method == 'POST':
        product_data = JSONParser().parse(request)
        product_serializer = ProducModelSerializer(data=product_data)
        if product_serializer.is_valid():
            product_serializer.save()
            return JsonResponse(product_serializer.data, status=status.HTTP_201_CREATED)
        return JsonResponse(product_serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    elif request.method == 'DELETE':
        count = ProductModel.objects.all().delete()
        return JsonResponse({'message': 'All products were deleted successfully!'.format(count[0])},
                            status=status.HTTP_204_NO_CONTENT)


@api_view(['GET', 'PUT', 'DELETE'])
def product_detail(request, pk):
    try:
        product = ProductModel.objects.get(pk=pk)
    except ProductModel.DoesNotExist:
        return JsonResponse({'message':'This product does not exist'}, status=status.HTTP_404_NOT_FOUND)

    if request.method == 'GET':
        product_serializer = ProductModel(product)
        return JsonResponse(product_serializer.data)

    elif request.method == 'PUT':
        product_data = JSONParser().parse(request)
        product_serializer = ProducModelSerializer(product, data=product_data)
        if product_serializer.is_valid():
            product_serializer.save()
            return JsonResponse(product_serializer.data)
        return JsonResponse(product_serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    elif request.method == 'DELETE':
        product.delete()
        return JsonResponse({'message':'Product was deleted succesfully!'}, status=status.HTTP_204_NO_CONTENT)


@api_view(['GET'])
def product_list_by_category(request, category_id):
    products = ProductModel.objects.all()

    try:
        products = ProductModel.objects.get(category=category_id)
    except ProductModel.DoesNotExist:
        category = request.GET.get('category', None)
        if category is not None:
            products = products.filter(category__icontains=category)
        else:
            return JsonResponse({'message': 'This product does not exist'}, status=status.HTTP_404_NOT_FOUND)

    product_serializer = ProducModelSerializer(products, many=True)
    return JsonResponse(product_serializer.data, safe=False)


@api_view(['GET'])
def product_list_by_feature(request, feature_id):
    products = ProductModel.objects.all()

    try:
        products = ProductModel.objects.get(feature=feature_id)
    except ProductModel.DoesNotExist:
        feature = request.GET.get('feature', None)
        if feature is not None:
            products = products.filter(feature__icontains=feature)
        else:
            return JsonResponse({'message': 'This product does not exist'}, status=status.HTTP_404_NOT_FOUND)

    product_serializer = ProducModelSerializer(products, many=True)
    return JsonResponse(product_serializer.data, safe=False)


@api_view(['GET', 'POST', 'DELETE'])
def category_list(request):
    if request.method == 'GET':
        categories = ProductCategoryModel.objects.all()

        title = request.GET.get('title', None)
        if title is not None:
            categories = categories.filter(title__icontains=title)

        category_serializer = ProductCategoryModelSerializer(categories, many=True)
        return JsonResponse(category_serializer.data, safe=False)

    elif request.method == 'POST':
        category_data = JSONParser().parse(request)
        category_serializer = ProductCategoryModelSerializer(data=category_data)
        if category_serializer.is_valid():
            category_serializer.save()
            return JsonResponse(category_serializer.data, status=status.HTTP_201_CREATED)
        return JsonResponse(category_serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    elif request.method == 'DELETE':
        count = ProductCategoryModel.objects.all().delete()
        return JsonResponse({'message': 'All products categories were deleted successfully!'.format(count[0])},
                            status=status.HTTP_204_NO_CONTENT)


@api_view(['GET', 'PUT', 'DELETE'])
def category_detail(request, pk):
    try:
        category = ProductCategoryModel.objects.get(pk=pk)
    except ProductCategoryModel.DoesNotExist:
        return JsonResponse({'message': 'This category does not exist'}, status=status.HTTP_404_NOT_FOUND)

    if request.method == 'GET':
        category_serializer = ProductCategoryModel(category)
        return JsonResponse(category_serializer.data)

    elif request.method == 'PUT':
        category_data = JSONParser().parse(request)
        category_serializer = ProductCategoryModelSerializer(category, data=category_data)
        if category_serializer.is_valid():
            category_serializer.save()
            return JsonResponse(category_serializer.data)
        return JsonResponse(category_serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    elif request.method == 'DELETE':
        category.delete()
        return JsonResponse({'message': 'Product category was deleted succesfully!'}, status=status.HTTP_204_NO_CONTENT)


@api_view(['GET', 'POST', 'DELETE'])
def feature_list(request):
    if request.method == 'GET':
        features = ProductFeatureModel.objects.all()

        title = request.GET.get('title', None)
        if title is not None:
            features = features.filter(title__icontains=title)

        feature_serializer = ProductFeatureModelSerializer(features, many=True)
        return JsonResponse(feature_serializer.data, safe=False)

    elif request.method == 'POST':
        feature_data = JSONParser().parse(request)
        feature_serializer = ProductFeatureModelSerializer(data=feature_data)
        if feature_serializer.is_valid():
            feature_serializer.save()
            return JsonResponse(feature_serializer.data, status=status.HTTP_201_CREATED)
        return JsonResponse(feature_serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    elif request.method == 'DELETE':
        count = ProductFeatureModel.objects.all().delete()
        return JsonResponse({'message': 'All products features were deleted successfully!'.format(count[0])},
                            status=status.HTTP_204_NO_CONTENT)


@api_view(['GET', 'PUT', 'DELETE'])
def feature_detail(request, pk):
    try:
        feature = ProductFeatureModel.objects.get(pk=pk)
    except ProductFeatureModel.DoesNotExist:
        return JsonResponse({'message': 'This feature does not exist'}, status=status.HTTP_404_NOT_FOUND)

    if request.method == 'GET':
        feature_serializer = ProductFeatureModel(feature)
        return JsonResponse(feature_serializer.data)

    elif request.method == 'PUT':
        feature_data = JSONParser().parse(request)
        feature_serializer = ProductFeatureModelSerializer(feature, data=feature_data)
        if feature_serializer.is_valid():
            feature_serializer.save()
            return JsonResponse(feature_serializer.data)
        return JsonResponse(feature_serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    elif request.method == 'DELETE':
        feature.delete()
        return JsonResponse({'message': 'Product feature was deleted succesfully!'}, status=status.HTTP_204_NO_CONTENT)
