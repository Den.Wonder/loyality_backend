from rest_framework import serializers
from products.models import ProductModel, ProductFeatureModel, ProductCategoryModel


class ProducModelSerializer(serializers.ModelSerializer):
    class Meta:
        model = ProductModel
        fields = (
            'id',
            'title',
            'value',
            'measuring',
            'trade_price',
            'retail_price',
            'max_discount_percent',
            'category',
            'feature'
        )


class ProductFeatureModelSerializer(serializers.ModelSerializer):
    class Meta:
        model = ProductFeatureModel
        fields = (
            'id',
            'title',
            'description'
        )


class ProductCategoryModelSerializer(serializers.ModelSerializer):
    class Meta:
        model = ProductCategoryModel
        fields = (
            'id',
            'title',
            'description'
        )
